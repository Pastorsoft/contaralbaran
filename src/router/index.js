const express = require("express");
const Router = express.Router();

const CabeceraAlbaranCliente = require("../controllers/CabeceraAlbaranCliente_Controller");
const LineasAlbaranCliente = require("../controllers/LineasAlbaranCliente_Controller");

Router.get("/CabeceraAlbaranPedido", CabeceraAlbaranCliente);
Router.get("/LineasAlbaranCliente", LineasAlbaranCliente);

module.exports = Router;
