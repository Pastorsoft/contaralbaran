const conexion = () => {
  const express = require("express");
  const Router = require("./router");
  const app = express();
  const port = 3000;

  app.use("/", Router);

  this.server = app.listen(port, () =>
    console.log(`Example app listening on port ${port}!`)
  );
};

const cerrarConexion = () => {
  this.server.close();
};

module.exports = { conexion, cerrarConexion };
