const sql = require("mssql");
const { conectar } = require("../../conexion/conectar");

const user = document.getElementById("user").value;
const password = document.getElementById("pass").value;
const server = document.getElementById("sql").value;
const database = document.getElementById("bd").value;

function LineasAlbaranCliente(req, res) {
  let TablaReferenciasDocumento = [];
  const consulta = async () => {
    try {
      const config = await conectar(user, password, server, database);

      await sql.connect(config);
      const resp = await sql.query`select * from LineasAlbaranCliente`;

      resp.recordset.map((element, i) => {
        TablaReferenciasDocumento.push({
          id: i,
          CodigoEmpresa: element.CodigoEmpresa,
          EjercicioAlbaran: element.EjercicioAlbaran,
          CodigoCliente: element.CodigoCliente,
          SerieAlbaran: element.SerieAlbaran,
          NumeroAlbaran: element.NumeroAlbaran,
          FechaAlbaran: element.FechaAlbaran,
          CodigoArticulo: element.CodigoArticulo,
          DescripcionArticulo: element.DescripcionArticulo,
          // Nombre: element.Nombre,
        });
      });
      res.json(TablaReferenciasDocumento);
    } catch (error) {
      console.log(error);
    }
  };
  consulta();
}

module.exports = LineasAlbaranCliente;
