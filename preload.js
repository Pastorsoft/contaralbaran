const { conexion, cerrarConexion } = require("./src/app");
const { conectar, config } = require("./conexion/conectar");

window.addEventListener("DOMContentLoaded", () => {
  const { writeFile } = require("fs");

  function escribir() {
    writeFile("config.cfg", sql.value, "utf8", (error) => {
      if (error) throw error;
      console.log("j");
    });
  }

  const es = document.getElementById("escribir");

  es.addEventListener("click", (e) => {
    escribir();
  });

  const con = document.getElementById("conexion");

  con.addEventListener("click", (e) => {
    const sql = require("mssql");
    const user = document.getElementById("user").value;
    const password = document.getElementById("pass").value;
    const server = document.getElementById("sql").value;
    const database = document.getElementById("bd").value;

    const consulta = async () => {
      try {
        const config = await conectar(user, password, server, database);

        await sql.connect(config);
        conexion();
      } catch (error) {
        console.log(error);
      }
    };
    consulta();
  });

  const cerrar = document.getElementById("cerrar");

  cerrar.addEventListener("click", (e) => {
    window.close();
  });

  const ceConexion = document.getElementById("cerrarConexion");

  ceConexion.addEventListener("click", (e) => {
    cerrarConexion();
  });
});
