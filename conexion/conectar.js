const sql = require("mssql");

const conectar = (user, password, server, database) => {
  const config = {
    user,
    password,
    server,
    database,
  };
  const poolPromise = new sql.ConnectionPool(config)
    .connect()
    .then((pool) => {
      document.getElementById("respuesta").value = "conect";
      console.log("Connected to MSSQL");

      return pool;
    })
    .catch((err) => {
      document.getElementById("respuesta").value = "error";
      console.log("Database Connection Failed! Bad Config: ", err);
    });
  return config;
};
module.exports = { conectar };
